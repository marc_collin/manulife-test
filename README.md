# Manulife-Test

It's a program done for a manulife interview.

User can enter a date and top 5 web site with more visit will be displayed.
User can enter web site url to display all visit for this web site.

Search is done on the blur event. 

### Prerequisites

JDK 8  
Gradle  
Mysql

Theses tools need to be installed.

### Installing

A database need to be created.  
The database can be modified in the application.properties file.  
Default name is: manulife  

CSV File need to be modified also in the application.properties file.

### Running

When project is build, an manulifetest-version.jar file is created.  
To run application: java -jar manulifetest-version.jar   

## Authors

* **Marc Collin** - *Initial work* - [Home site](http://www.laboiteaprog.com)

## License
GPL

## Improvement

Instead of import automatically csv file every time when program start, another service could be created only for that.   
So service to get data will easier to put in the cloud like amazon. Many instance of the service could be start.  
React UI could be better. Actually same input text is used for date and url. A datetimepicker could be used and input text could be used for the other parameter.  
Validation could be add on the user input.