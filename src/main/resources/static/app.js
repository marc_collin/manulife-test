var WebSite = React.createClass({

    getInitialState: function () {
        return {display: true};
    },
    render: function () {

        if (this.state.display == false)
            return null;
        else
            return (
                    <tr>
                        <td>{this.props.website.webSite}</td>
                        <td>{this.props.website.date}</td>
                        <td>{this.props.website.visits}</td>
                    </tr>
                    );
    }
});

var WebSiteTable = React.createClass({

    render: function () {

        var rows = [];
        this.props.websites.forEach(function (website) {
            rows.push(
                    <WebSite website={website} key={website.website} />);
        });

        return (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Website</th>
                            <th>Date</th>
                            <th>Visits</th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                );
    }
});

var App = React.createClass({

    getInitialState: function () {
        return {websites: []};
    },

    onBlur: function (event) {
        var self = this;
        self.setState({websites: []});

        var searchValue = event.target.value;

        var baseUrl = "http://localhost:8080/rest/report/";
        var url = baseUrl + "statwebsite?website=" + searchValue;

        if (moment(searchValue).isValid()) {
            url = baseUrl + "topwebsitebydate?date=" + searchValue;
        }

        $.ajax({
            url: url,
        }).then(function (data) {
            self.setState({websites: data});
        });
    },
    render: function () {

        return (
                <section>
                    <br/>
                    <input type="text" onBlur={ this.onBlur } placeholder="Enter your date or website here."/>
                    <WebSiteTable websites={this.state.websites} />
                </section>

                );
    }

});

ReactDOM.render(<App />, document.getElementById('root'));
