/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.dto;

import java.time.LocalDate;

/**
 *
 * @author collinm
 */
public class WebSiteDto {
    private LocalDate date;
    private String webSite;
    private int visits;

    public WebSiteDto(){
        
    }
    
    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the webSite
     */
    public String getWebSite() {
        return webSite;
    }

    /**
     * @param webSite the webSite to set
     */
    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    /**
     * @return the visits
     */
    public int getVisits() {
        return visits;
    }

    /**
     * @param visits the visits to set
     */
    public void setVisits(int visits) {
        this.visits = visits;
    }
}
