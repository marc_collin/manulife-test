/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.controller;

import com.manulife.dto.WebSiteDto;
import com.manulife.service.IReportService;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author collinm
 */
@RequestMapping(value="/rest/report")
@RestController
public class ReportWebSiteController {
    private final IReportService reportService;

    @Autowired
    public ReportWebSiteController(final IReportService reportService) {
        this.reportService = reportService;
    }
    
    @GetMapping(value = "/topwebsitebydate")
    public List<WebSiteDto> getTopWebSiteByDate(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date, @RequestParam(required = false,  defaultValue = "5") int max) {
        return reportService.getTopWebSite(date, max);
    }
    
    @GetMapping(value = "/statwebsite")
    public List<WebSiteDto> getStatWebSite(String website) {
        return reportService.getStatWebSite(website);
    }
    
    @GetMapping(value = "/statwebsiteg")
    public List<WebSiteDto> getStatWebSiteGoogle() {
        return reportService.getStatWebSite("www.google.com");
    }
}
