/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife;

import com.manulife.service.CsvFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author collinm
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

     @Value("${csv.file.stat}")
    private String csvFileStat;
    
     @Autowired
     private CsvFileReader csvFileReader;
     
    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     * @param event
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        
       csvFileReader.readCsvFile(csvFileStat);
        
        // here your code ...
        return;
    }
}
