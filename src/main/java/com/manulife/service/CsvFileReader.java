/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.service;

import com.manulife.bean.WebSite;
import com.manulife.repository.WebSiteRepository;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author collinm
 */
@Service
public class CsvFileReader {

    //CSV file header
    private static final String[] FILE_HEADER_MAPPING = {"date", "website", "visits"};

     private final WebSiteRepository webSiteRepository;

    @Autowired
    public CsvFileReader(final WebSiteRepository webSiteRepository) {
        this.webSiteRepository = webSiteRepository;
    }
    
    public void readCsvFile(String fileName) {

        FileReader fileReader = null;

        CSVParser csvFileParser = null;

        //Create the CSVFormat object with the header mapping
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING).withDelimiter('|');

        try {

            //Create a new list of student to be filled by CSV file data 
            List<WebSite> websites = new ArrayList<>();

            //initialize FileReader object
            fileReader = new FileReader(fileName);

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = (CSVRecord) csvRecords.get(i);
                //Create a new student object and fill his data
                WebSite webSite = new WebSite();
                webSite.setDate(LocalDate.parse(record.get(0)));
                webSite.setWebSite(record.get(1));
                webSite.setVisits(Integer.parseInt(record.get(2)));
                websites.add(webSite);
            }

            saveWebSite(websites);
            
        } catch (IOException | NumberFormatException e) {
            System.out.println("Error in CsvFileReader !!!");
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser !!!");
            }
        }

    }
    
    @Transactional
    private void saveWebSite(List<WebSite> websites){
         webSiteRepository.save(websites);
    }

}
