/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.service;

import com.manulife.bean.WebSite;
import com.manulife.dto.WebSiteDto;
import com.manulife.repository.WebSiteRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author collinm
 */
@Service
public class ReportService implements IReportService {

    private final WebSiteRepository webSiteRepository;

    @Autowired
    public ReportService(final WebSiteRepository webSiteRepository) {
        this.webSiteRepository = webSiteRepository;
    }

    @Override
    public List<WebSiteDto> getTopWebSite(LocalDate date, int max) {
        if (max < 5) {
            max = 5;
        }
        List<WebSite> webSites = webSiteRepository.getTopWebSiteByDate(date, new PageRequest(0, max));
        List<WebSiteDto> webSitesDto = new ArrayList<>();
        for (WebSite webSite : webSites) {
            WebSiteDto webSiteDto = new WebSiteDto();

            BeanUtils.copyProperties(webSite, webSiteDto);
            webSitesDto.add(webSiteDto);
        }

        return webSitesDto;
    }

    @Override
    public List<WebSiteDto> getStatWebSite(String website) {
        List<WebSite> webSites = webSiteRepository.findByWebSite(website);

        List<WebSiteDto> webSitesDto = new ArrayList<>();
        for (WebSite webSite : webSites) {
            WebSiteDto webSiteDto = new WebSiteDto();

            BeanUtils.copyProperties(webSite, webSiteDto);
            webSitesDto.add(webSiteDto);
        }

        return webSitesDto;
    }

}
