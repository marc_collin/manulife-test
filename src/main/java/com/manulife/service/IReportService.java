/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.service;

import com.manulife.dto.WebSiteDto;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author collinm
 */
public interface IReportService {
    public List<WebSiteDto> getTopWebSite(LocalDate date, int max);

    public List<WebSiteDto> getStatWebSite(String website);
}
