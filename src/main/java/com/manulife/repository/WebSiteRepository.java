/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.repository;

import com.manulife.bean.WebSite;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author collinm
 */
public interface WebSiteRepository extends JpaRepository< WebSite, Long>{
    
    @Query("select w from WebSite w where w.date=:date order by w.visits")
    public List<WebSite> getTopWebSiteByDate(LocalDate date, Pageable pageable);
    
    public List<WebSite> findByWebSite(String webSite);
    
}
