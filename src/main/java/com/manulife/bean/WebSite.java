/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manulife.bean;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author collinm
 */

@Entity
public class WebSite implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long webSiteId;
    
    private LocalDate date;
    private String webSite;
    private int visits;
    
    public WebSite(){
        
    }

    /**
     * @return the webSiteId
     */
    public Long getWebSiteId() {
        return webSiteId;
    }

    /**
     * @param webSiteId the webSiteId to set
     */
    public void setWebSiteId(Long webSiteId) {
        this.webSiteId = webSiteId;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the webSite
     */
    public String getWebSite() {
        return webSite;
    }

    /**
     * @param webSite the webSite to set
     */
    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    /**
     * @return the visits
     */
    public int getVisits() {
        return visits;
    }

    /**
     * @param visits the visits to set
     */
    public void setVisits(int visits) {
        this.visits = visits;
    }

  
}
